import cv2
import numpy as np
from itertools import product
import pickle

from matrices_reader import *

PICKLE_PATH = "/tmp/pickle.truc"

VOXEL_SIZE = 2e-2
X_MIN, X_MAX = -2.0, 2.0 
Y_MIN, Y_MAX = -2.0, 2.0
Z_MIN, Z_MAX = -2.0, 2.0

with open(PICKLE_PATH, 'rb') as file:
    projection_matrices = pickle.load(file)
nb_frame = len(projection_matrices) - 1

points = np.array([[x, y, z, 1.0] for x, y, z in product(
    np.arange(X_MIN, X_MAX, VOXEL_SIZE),
    np.arange(Y_MIN, Y_MAX, VOXEL_SIZE),
    np.arange(Z_MIN, Z_MAX, VOXEL_SIZE))])

points_tor = np.array([[np.cos(theta), np.sin(theta), 0.0, 1.0] for theta, phi, r in product(
    np.arange(0, 2*np.pi, 0.1),
    np.arange(0, 2*np.pi, 0.1),
    np.arange(0.0, 0.25, 0.1))])

mask = np.array([255, 255, 255])
# is_in = []
# p = [0.0, 1.1, 0.0]

for k in range(nb_frame):
    # points = np.array([p +[1.0]])

    frame = cv2.imread(f'/tmp/masks/Image{k+1:04}.png')
    proj_mat = projection_matrices[k]
    
    cam_points = proj_mat @ points.T
    cam_points /= cam_points[2,:]
    cam_points = np.round(cam_points).astype(np.int32)

    visible = np.logical_and.reduce((0 <= cam_points[0,:], cam_points[0,:] < frame.shape[1], 0 <= cam_points[1,:], cam_points[1,:] < frame.shape[0]))
    cam_points = cam_points[:,visible]
    points = points[visible,:]

    solid = (frame[cam_points[1,:],cam_points[0,:]] == mask).all(axis=1)
    cam_points = cam_points[:,solid]
    points = points[solid,:]

    # is_in.append(len(points) != 0)

# points = np.array([p + [1.0]])

for k in range(nb_frame):
    frame = cv2.imread(f'/tmp/masks/Image{k+1:04}.png')
    proj_mat = projection_matrices[k]
    
    cam_points = proj_mat @ points.T
    cam_points /= cam_points[2,:]
    cam_points = np.round(cam_points).astype(np.int32)

    for cam_point in cam_points.T:
        # cv2.circle(frame, (cam_point[0], cam_point[1]), 2, (255*is_in[k], 0, 255*(not is_in[k])))
        cv2.circle(frame, (cam_point[0], cam_point[1]), 2, (255, 0, 255))

    cv2.imshow('Frame', frame)
    cv2.waitKey(0)