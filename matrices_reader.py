import re
import numpy as np


def matrices_reader(path: str) -> list[np.ndarray]:
    """Read projection matrices.

    Args:
        path (str): path to matrices.txt

    Returns:
        list[np.ndarray]: list of projection matrix
    """

    with open(path, 'r') as f:
        lines = f.readlines()

    k = 0
    world_matrices = []
    while k+3 < len(lines):
        # Match matrices one by one
        mat_str = ""
        for line in lines[k:k+4]:
            mat_str += line
        float_reg = r"(-|\d|\.|e)+"
        res = re.search(
            f"Matrix\(\(\(({float_reg}), ({float_reg}), ({float_reg}), ({float_reg})\),\n +\(({float_reg}), ({float_reg}), ({float_reg}), ({float_reg})\),\n\ +\(({float_reg}), ({float_reg}), ({float_reg}), ({float_reg})\)\)\)", mat_str)
        
        # Convert string to np.ndarray
        values = [float(res.group(i)) for i in range(1,len(res.groups()) + 1, 2)]
        world_mat = np.array([[values[4*i + j] for j in range(4)] for i in range(3)])
        world_matrices.append(world_mat)
        
        k += 4
    
    return world_matrices[1:]
